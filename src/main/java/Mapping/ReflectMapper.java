package Mapping;

import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.ClassWriter;

import java.io.IOException;
import java.io.InputStream;
import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.invoke.MethodType;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.UndeclaredThrowableException;
import java.util.HashMap;
import java.util.function.Function;

public class ReflectMapper<TSource extends Object, TDest extends Object> extends MapFunctionGeneratorBase implements IMapper<TSource, TDest> {
    final private Class<TDest> destType;
    final private Class<TSource> sourceType;

    public ReflectMapper(Class<TDest> destType, Class<TSource> sourceType) {
        super();
        this.destType = destType;
        this.sourceType = sourceType;
    }


    public Function<TSource, TDest> getMapFunction() {
        final Method[] setters = getSetters(destType);
        final Constructor<TDest> con = getConstructor(destType);

        Function<TSource, TDest> mapFunc =  (TSource src) -> {
            TDest newInstance;
            try {
                newInstance = con.newInstance();
            } catch (IllegalAccessException | InstantiationException | InvocationTargetException ex) {
                throw new IllegalArgumentException("Unable to instantiate" + destType.getName());
            }


            for (int i = 0; i < setters.length; i++) {
                Method setter = setters[i];
                String propertyName = setter.getName().substring(3);
                Method getter;

                try {
                    getter = sourceType.getMethod("get" + propertyName);
                } catch (NoSuchMethodException ex) {
                    throw new IllegalArgumentException(String.format("property getter for %s could not be found on source type", propertyName));
                }

                getter.setAccessible(true);

                try {
                    setters[i].invoke(newInstance, getter.invoke(src));

                } catch (IllegalAccessException | InvocationTargetException ex) {
                    System.out.println(ex);
                }

            }
            return newInstance;
        };

        return mapFunc;
    }

    class MapClassLoader extends ClassLoader {

        public Class<?> defineClass(byte[] bytes, String name) {
            return super.defineClass(name, bytes, 0, bytes.length);
        }
    }
}