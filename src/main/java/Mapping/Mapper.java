package Mapping;

import Mapping.Strategies.IMapStrategy;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.Type;
import org.objectweb.asm.signature.SignatureVisitor;
import org.objectweb.asm.signature.SignatureWriter;

import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.invoke.MethodType;
import java.lang.reflect.UndeclaredThrowableException;
import java.util.function.Function;

public class Mapper<TSource, TDest> implements IMapper<TSource, TDest> {
    private IMapStrategy strategy;
    private Class<TDest> destType;
    private Class<TSource> sourceType;

    private static MapFunctionCache mapCache = new MapFunctionCache();

    public Mapper(IMapStrategy strategy, Class<TDest> destType, Class<TSource> sourceType) {
        this.strategy = strategy;
        this.destType = destType;
        this.sourceType = sourceType;
    }

    public Function<TSource, TDest> getMapFunction() {

        if(mapCache.containsKey(sourceType, destType, strategy)) {
            return mapCache.get(sourceType, destType, strategy);
        }

        ClassWriter writer = new ClassWriter(ClassWriter.COMPUTE_FRAMES);
        String className = String.format("%s|%s|%s|Mapper", destType.getName(), sourceType.getName(), strategy.getClass().getName());
        className = className.replace('.', '\\');
        writer.visit(Opcodes.V12, Opcodes.ACC_PUBLIC, className, null, "java/lang/Object", null);

        String destDescript = Type.getDescriptor(destType);
        String sourceDescript = Type.getDescriptor(sourceType);
        String methodDescript = String.format("(%s)%s", sourceDescript, destDescript);

        SignatureWriter signature = new SignatureWriter();
        signature.visitFormalTypeParameter("TDest");
        {
            SignatureVisitor classBound = signature.visitClassBound();
            classBound.visitClassType(Type.getInternalName(Object.class));
            classBound.visitEnd();
        }
        signature.visitParameterType().visitTypeVariable("TSource");
        signature.visitReturnType().visitFormalTypeParameter("TDest");
        signature.visitEnd();
        String signatureString = signature.toString();

        MethodVisitor mv = writer.visitMethod(Opcodes.ACC_PUBLIC + Opcodes.ACC_STATIC, "map", methodDescript, signatureString, null);
        strategy.execute(mv, destType, sourceType);

        byte[] byteCode = writer.toByteArray();

        MapClassLoader loader = new MapClassLoader();
        Class clazz = loader.defineClass(byteCode, className);
        MethodHandles.Lookup lookup = MethodHandles.publicLookup();
        MethodType mt = MethodType.methodType(destType, sourceType);
        MethodHandle mapHandle;
        try {
            mapHandle = lookup.findStatic(clazz, "map", mt);
        } catch (Exception ex) {
            throw new UndeclaredThrowableException(ex);
        }
        Function<TSource, TDest> mapFunc =  (TSource src) -> {
            try {
                return (TDest) mapHandle.invoke(src);
            } catch (Throwable throwable) {
                throwable.printStackTrace();
                throw new UndeclaredThrowableException(throwable);
            }
        };

        mapCache.put(destType, sourceType, strategy, mapFunc);
        return mapFunc;
    }
    class MapClassLoader extends ClassLoader {

        Class<?> defineClass(byte[] bytes, String name) {
            return super.defineClass(name, bytes, 0, bytes.length);
        }
    }
}
