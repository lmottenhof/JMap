package Mapping;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashMap;
import java.util.stream.Stream;

import org.objectweb.asm.*;


public abstract class MapFunctionGeneratorBase {
    protected static final HashMap<Class, Method[]> cachedSetters = new HashMap<>();
    protected static final HashMap<Class, Constructor> cachedConstructors = new HashMap<>();

    /*
     * Retrieves the parameterless constructor of a class.
     * */
    protected static <TDest> Constructor<TDest> getConstructor(Class<TDest> destType) {
        Constructor<TDest> con;
        if (cachedConstructors.containsKey(destType)) {
            con = cachedConstructors.get(destType);
        } else {
            try {
                con = destType.getDeclaredConstructor();
                con.setAccessible(true);
                cachedConstructors.put(destType, con);
            } catch (NoSuchMethodException ex) {
                throw new IllegalArgumentException("Destination type requires parameterless constructor");
            }
        }
        return con;
    }

    /*
     * Retrieve the setters of a class.
     * */
    protected static <TDest> Method[] getSetters(Class<TDest> destType) {
        Method[] setters;
        if (cachedSetters.containsKey(destType)) {
            setters = cachedSetters.get(destType);
        } else {
            Method[] methods = destType.getDeclaredMethods();
            Stream<Method> stream = Arrays.stream(methods).filter(m -> m.getName().startsWith("set"));
            setters = stream.map(m -> {
                m.setAccessible(true);
                return m;
            }).toArray(Method[]::new);
            cachedSetters.put(destType, setters);
        }
        return setters;
    }
}
