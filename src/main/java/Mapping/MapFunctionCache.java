package Mapping;

import Mapping.Strategies.IMapStrategy;

import java.util.HashMap;
import java.util.function.Function;

class MapFunctionCache {
    private HashMap<Key, Function> cachedFunctions = new HashMap<>();

    <TSource, TDest> Function<TSource, TDest> get(Class<TSource> sourceType, Class<TDest> destType, IMapStrategy strat) {
        Key key = new Key(sourceType, destType, strat);
        if(cachedFunctions.containsKey(key)) {
            return cachedFunctions.get(key);
        }
        return null;
    }

    <TSource, TDest> void put(Class<TSource> src, Class<TDest> dest, IMapStrategy strat, Function mapFunc) {
        cachedFunctions.put(new Key(dest, src, strat), mapFunc);
    }

    <TSource, TDest> boolean containsKey(Class<TSource> src, Class<TDest> dest, IMapStrategy strat) {
        return cachedFunctions.containsKey(new Key(dest, src, strat));
    }


}

class Key<TSource, TDest> {
    private Class<TDest> dest;
    private Class<TSource> src;
    private IMapStrategy strategy;
    Key(Class<TDest> destType, Class<TSource> sourceType, IMapStrategy strategy) {
        dest = destType;
        src = sourceType;
        this.strategy = strategy;
    }

    public Class<TDest> getKey() {
        return dest;
    }

    public Class<TSource> getValue() {
        return src;
    }

    @Override
    public boolean equals(Object obj) {
        if(!(obj instanceof Key)) return false;
        Key key = (Key)obj;
        return key.dest.getName() == this.dest.getName() && key.src.getName() == this.src.getName() && this.strategy.getClass().getName() == key.strategy.getClass().getName();
    }

    @Override
    public int hashCode() {
        return dest.hashCode() ^ src.hashCode() ^ strategy.hashCode();
//        final int prime = 31;
//        int result = 1;
//        result = prime * result + ((dest == null) ? 0 : dest.hashCode());
//        result = prime * result + ((src == null) ? 0 : src.hashCode());
//        return result;
    }

}