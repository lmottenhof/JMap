package Mapping;

import java.util.function.Function;

public interface IMapper<TSource, TDest> {

    Function<TSource, TDest> getMapFunction();
}
