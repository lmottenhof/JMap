package Mapping;

import Mapping.Strategies.DefaultMapStrategy;
import Mapping.Strategies.IMapStrategy;

public class MapFactory {

    public static <TSource, TDest> IMapper getDefaultMapper(Class<TDest> dest, Class<TSource> src) {
        return new Mapper(new DefaultMapStrategy(), dest, src);
    }

    public static <TSource, TDest> IMapper getMapper(IMapStrategy strategy, Class<TDest> dest, Class<TSource> src) {
        return new Mapper(strategy, dest, src);
    }

    public static <TSource, TDest> TDest map(Class<TDest> destType, Class<TSource> sourceType, TSource source) {
        return (TDest)new Mapper(new DefaultMapStrategy(), sourceType, destType).getMapFunction().apply(source);
    }


}
