package Mapping.Strategies;

import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.Type;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.stream.Stream;

public class DefaultMapStrategy<TSource, TDest> implements IMapStrategy<TSource, TDest> {
    public void execute(MethodVisitor mv, Class<TDest> destType, Class<TSource> sourceType) {

        mv.visitCode();
        //Create a new instance of the destination type. It is uninitialised
        mv.visitTypeInsn(Opcodes.NEW, Type.getInternalName(destType));
        //Duplicate the new object
        mv.visitInsn(Opcodes.DUP);
        //Initialize the object by invoking the constructor of the destination type.
        mv.visitMethodInsn(Opcodes.INVOKESPECIAL, Type.getInternalName(destType), "<init>", "()V", false);
        //Load the first argument, which is a object of the source type.
        mv.visitVarInsn(Opcodes.ALOAD, 0);

        Method[] methods = destType.getMethods();
        //Filter the setters from the destination type.
        Stream<Method> stream = Arrays.stream(methods).filter(m -> m.getName().startsWith("set")).map(m -> {m.setAccessible(true); return m;});
        for(Method setMethod : stream.toArray(Method[]::new)) {
            Method getMethod;
            try {
                //Retrieve a get method from the source type based on the name of the getter
                getMethod = sourceType.getMethod("get" + setMethod.getName().substring(3));
            } catch(NoSuchMethodException ex) {
                throw new IllegalArgumentException("Could not find property", ex);
            }
            //Duplicate the top 2 values on the stack, which are the source and destination objects,
            mv.visitInsn(Opcodes.DUP2);
            //Invoke the getter. The the source object is removed and the result is placed on the stack
            mv.visitMethodInsn(Opcodes.INVOKEVIRTUAL, Type.getInternalName(sourceType), getMethod.getName(), Type.getMethodDescriptor(getMethod), false);
            //Set the property through it's setter, removing the destination object along with the previous result from the stack
            mv.visitMethodInsn(Opcodes.INVOKEVIRTUAL, Type.getInternalName(destType), setMethod.getName(), Type.getMethodDescriptor(setMethod), false);
        }
        //Remove the original source object.
        mv.visitInsn(Opcodes.POP);
        //Return the new destination object
        mv.visitInsn(Opcodes.ARETURN);
        //Specify the maximum stack size and local variables
        mv.visitMaxs(4, 1);
        mv.visitEnd();
    }
}
