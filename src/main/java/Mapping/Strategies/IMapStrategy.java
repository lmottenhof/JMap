package Mapping.Strategies;

import org.objectweb.asm.MethodVisitor;

public interface IMapStrategy<TSource, TDest> {
    void execute(MethodVisitor method, Class<TDest> destType, Class<TSource> sourceType);
}
