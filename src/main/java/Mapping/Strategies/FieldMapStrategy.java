package Mapping.Strategies;

import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.Type;

import java.lang.reflect.Field;

//Doesn't actually work
public class FieldMapStrategy implements IMapStrategy{
    @Override
    public void execute(MethodVisitor method, Class destType, Class sourceType) {
        method.visitCode();
        method.visitTypeInsn(Opcodes.NEW, Type.getInternalName(destType));
        method.visitInsn(Opcodes.DUP);
        //Invoke the constructor of the destination type to create a new instance of the object.
        method.visitMethodInsn(Opcodes.INVOKESPECIAL, Type.getInternalName(destType), "<init>", "()V", false);
        //Load the first argument, which is a object of the source type.
        method.visitVarInsn(Opcodes.ALOAD, 0);

        for (Field destField : destType.getDeclaredFields()) {
            Field sourceField;
            try {
                sourceField = sourceType.getDeclaredField(destField.getName());
            } catch (NoSuchFieldException ex) {
                throw new IllegalArgumentException("Could not find field", ex);
            }
            //Duplicate the top 2 values, which are the destination object and the source object
            method.visitInsn(Opcodes.DUP2);
            //Get the value from the source field. This removes the duplicated source object.
            method.visitFieldInsn(Opcodes.GETFIELD, Type.getInternalName(sourceType), sourceField.getName(), Type.getDescriptor(sourceField.getType()));
            //Set the destination field with the value from the source field. The value and destination object are removed.
            method.visitFieldInsn(Opcodes.PUTFIELD, Type.getInternalName(destType), destField.getName(), Type.getDescriptor(sourceField.getType()));
            //The original source and destination objects remain.
        }
        //Remove the original source object.
        method.visitInsn(Opcodes.POP);
        //Return the new destination object
        method.visitInsn(Opcodes.ARETURN);
        method.visitMaxs(4, 1);
    }
}
